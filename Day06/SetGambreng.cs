﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day06
{
    class SetGambreng
    {
        public static void Resolve ()
        {
            Console.WriteLine("Masukan ronde ");
            int ronde = int.Parse(Console.ReadLine());

            int menangA = 0;
            int kalahA = 0;
            int seriA= 0;

            int menangB = 0;
            int kalahB = 0;
            int seriB = 0;

            for (int i = 0; i < ronde; i++)
            {
                Console.WriteLine("Ronde Ke- " + (i + 1));

                Console.WriteLine("Masukan set gambreng A : ");
                string gambrengA = Console.ReadLine();

                Console.WriteLine("Masukan set gambreng B : ");
                string gambrengB = Console.ReadLine();

                char[] charGambrengA = gambrengA.ToCharArray();
                char[] charGambrengB = gambrengB.ToCharArray();

                for (int j = 0; j < gambrengB.Length; j++)
                {
                    if (charGambrengB[j] == 'B' && charGambrengA[j] == 'G')
                    {
                        menangB++;
                    }
                    else if (charGambrengB[j] == 'B' && charGambrengA[j] == 'K')
                    {
                        kalahB++;
                    }
                    else if (charGambrengB[j] == charGambrengA[j])
                    {
                        seriB++;
                    }

                }
                Console.Write("B Menang " + menangB + " Kalah " + kalahB + " seri " + seriB);
                Console.WriteLine();

                for (int j = 0; j < gambrengA.Length; j++)
                {
                    if (charGambrengA[j] == 'G' && charGambrengA[j] == 'B')
                    {
                        kalahA++;
                    }
                    else if (charGambrengA[j] == 'K' && charGambrengB[j] == 'B')
                    {
                        menangA++;
                    }
                    else if (charGambrengA[j] == charGambrengB[j])
                    {
                        seriA++;
                    }

                }
                Console.Write("Menang " + menangA + " Kalah " + kalahA + " seri " + seriA);
                Console.WriteLine();

                if(menangB > menangA)
                {
                    Console.WriteLine("B Menang");
                }
                else
                {
                    Console.WriteLine("A Menang");
                }
            }
        }
    }
}
