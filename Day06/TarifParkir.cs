﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day06
{
    class TarifParkir
    {
        public static void Resolve ()
        {
            //validasi kepenulisan waktu
            try
            {
                Console.WriteLine("Masukkan Waktu Masuk : ");
                string stringMasuk = Console.ReadLine();

                Console.WriteLine("Masukkan Waktu Keluar : ");
                string stringKeluar = Console.ReadLine();

                //konversi datetime
                DateTime waktuMasuk = Convert.ToDateTime(stringMasuk);
                DateTime waktuKeluar = Convert.ToDateTime(stringKeluar);

                double selisihHari = (waktuKeluar - waktuMasuk).TotalDays;

                double lamaParkir = selisihHari * (double)24;

                double biaya = 0;

                //validasi minus
                if (lamaParkir <= 0)
                {
                    Console.WriteLine("input yang anda masukkan salah!");
                }

                //Delapan jam pertama
                else if (lamaParkir <= 8)
                {
                    biaya = lamaParkir * 1000;
                    Console.WriteLine(biaya);
                }

                //Lebih dari 8 jam - 24 jam
                else if (lamaParkir > 8 && lamaParkir <= 24)
                {
                    biaya = 8000;
                    Console.WriteLine(biaya);
                }

                //Lebih dari 24 jam
                else if (lamaParkir > 24)
                {
                    biaya = 15000;
                    Console.WriteLine(biaya);
                }

            }
            catch (Exception)
            {
                //memunculkan error sistem pada console
                Console.WriteLine("Format waktu salah!");
            }
        }
    }
}
