﻿using System;

namespace Day05
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y") //answer to upper kalo user input huruf kecil akan diubah jadi kapital jd gak error
            {
                Console.WriteLine("Masukan nomor soal");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal) //untuk memilih apa inputan user
                {
                    case 1:
                        Console.WriteLine("Bilangan prima ");
                        BilanganPrima.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Es Loli ");
                        EsLoli.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Modus ");
                        Modus.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("Custom sort ");
                        CustomSort.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("Pustaka ");
                        Pustaka.Resolve();
                        break;

                    case 6:
                        Console.WriteLine("Kaca mata dan baju ");
                        KacaMataDanBaju.Resolve();
                        break;

                    case 7:
                        Console.WriteLine("Lilin Fibonacci ");
                        LilinFibonacci.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("Int Geser ");
                        IntGeser.Resolve();
                        break;

                    case 9:
                        Console.WriteLine("Parkir ");
                        Parkir.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("Naik gunung ");
                        NaikGunung.Resolve();
                        break;

                    case 11:
                        Console.WriteLine("Kaos kaki ");
                        KaosKaki.Resolve();
                        break;

                    case 12:
                        Console.WriteLine("Pembulatan nilai ");
                        PembulatanNilai.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan ");
                        break;
                }

                Console.WriteLine("Lanjutkan?");
                answer = Console.ReadLine();
            }
        }
    }
}
