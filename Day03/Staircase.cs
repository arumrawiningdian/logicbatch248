﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Staircase
    {
        public static void Resolve ()
        {
            Console.WriteLine("Input the n ");
            int length = int.Parse(Console.ReadLine());


            for (int i = 0; i <= length; i++)
            {
                for (int j = i; j < length; j++)
                {
                    Console.Write(" ");
                }
                for (int j = 1; j <= i; j++)
                {
                    Console.Write("#");
                }

                Console.WriteLine();
            }

        }
    }
}
