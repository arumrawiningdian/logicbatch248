﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class MiniMaxSum
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the set of number ");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);

            int maksimal = numbersArray.Max();
            int minimal = numbersArray.Min();
            int total = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                total += numbersArray[i];
            }

            Console.WriteLine("The total is " + total);

            //Perhitungan
            int sumMaks = total - minimal;
            int sumMin = total - maksimal;

            Console.WriteLine("Total bilangan yang terbesar " + sumMaks);
            Console.WriteLine("Total bilangan yang terkecil " + sumMin);
        }

    }
}
