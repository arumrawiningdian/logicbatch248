﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class DiagonalDifference
    {
        public static void Resolve()
        {
            Console.WriteLine("Input the length of the matrix ");
            int length = int.Parse(Console.ReadLine());

            int diagonalSatu = 0;
            int diagonalDua = 0;

            int[,] array2D = new int[length, length];

            for (int i = 0; i < length; i++)
            {
                Console.WriteLine("Enter the " + (i + 1) + " set of numbers");
                string numbers = Console.ReadLine();

                int[] array = Utility.ConvertStringToIntArray(numbers);
                if (array.Length != length)
                {
                    Console.WriteLine("Wrong numbers, try again");
                    i = -1;
                }
                else
                {
                    for (int j = 0; j < length; j++)
                    {
                        array2D[i, j] = array[j];
                    }
                    
                }
               
            }

            //Proses Inputan
            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    if (i == j)
                    {
                        diagonalSatu += array2D[i, j];
                    }
                    
                }
                
            }

            //Diagonal Atas Kanan Kiri
            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                for (int j = 0; j < array2D.GetLength(1); j++)
                {
                    if (i + j == array2D.GetLength(1) - 1)
                    {
                        diagonalDua += array2D[i, j];
                    }
                    
                }
            }

            Console.WriteLine();
            Console.WriteLine("Diagonal pertama " + diagonalSatu);
            Console.WriteLine("Diagonal kedua " + diagonalDua);
            Console.WriteLine("Hasilnya : " + Math.Abs(diagonalSatu - diagonalDua));
        }
    }
}
