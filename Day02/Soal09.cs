﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02
{
    class Soal09
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan nilai n");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan nilai n2");
            int n2 = int.Parse(Console.ReadLine());

            int[,] array2D = new int[3, n];

            //value creation
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        array2D[i, j] = j;
                    }
                    else if (i == 1)
                    {
                        if (j == 0) //jika i = 1 dan j = 0
                        {
                            array2D[i, j] = 0;
                        }
                        else //jika i = 1 dan j bukan 0 maka tambah berdasarkan nilai n2
                        {
                            array2D[i, j] = array2D[i, j - 1] + n2;
                        }
                    }
                    else
                    {
                        if (j == 0) //jika i = 2 dan j = 0 ambil dari nilai terakhir sebelumnya
                        {
                            array2D[i, j] = array2D[i - 1, j + (n - 1)];
                        }
                        else
                        {
                            array2D[i, j] = array2D[i, j - 1] - n2;
                        }
                    }
                }
            }
            Utility.PrintArray2D(array2D); //buat print
        }
    }
}
