﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PreTest
{
    class Bensin
    {
        public static void Resolve()
        {
            Console.WriteLine("Rute perjalanan ");
            Console.WriteLine("1. Toko ");
            Console.WriteLine("2. Tempat 1 ");
            Console.WriteLine("3. Tempat 2 ");
            Console.WriteLine("4. Tempat 3 ");
            Console.WriteLine("5. Tempat 4 ");

            Console.WriteLine("Masukan rute perjanan (Contoh : Toko-Tempat 1-Tempat 2-Toko) : ");
            string pilihan = Console.ReadLine();

            string[] pilihanArray = pilihan.Split('-');

            double tokoKeTempatSatu = 2;
            double tempatSatuKeDua = 0.5;
            double tempatDuaKeTiga = 1.5;
            double tempatTigaKeEmpat = 2.5;
            double bensin = 2.5;

            double jarak;

            switch (pilihan)
            {
                case "Toko":
                    jarak = 0;
                    double bensinToko = jarak / bensin;
                    Console.WriteLine(bensinToko);
                    break;

                case "Tempat 1":
                    jarak = tokoKeTempatSatu;
                    double bensinTempat1 = jarak / bensin;
                    Console.WriteLine(bensinTempat1);
                    break;

                case "Tempat 2":
                    jarak = tokoKeTempatSatu + tempatSatuKeDua;
                    break;

                case "Tempat 3":
                    jarak = tokoKeTempatSatu + tempatSatuKeDua + tempatDuaKeTiga;
                    break;

                case "Tempat 4":
                    jarak = tokoKeTempatSatu + tempatSatuKeDua + tempatDuaKeTiga + tempatTigaKeEmpat;
                    break;
            }
        }
    }
}
