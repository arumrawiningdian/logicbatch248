﻿using System;

namespace PreTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y") //answer to upper kalo user input huruf kecil akan diubah jadi kapital jd gak error
            {
                Console.WriteLine("Masukan nomor soal");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal) //untuk memilih apa inputan user
                {
                    case 1:
                        Console.WriteLine("Poin pulsa ");
                        PoinPulsa.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Bensin ");
                        Bensin.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Konversi volume ");
                        KonversiVolume.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("Porsi makan ");
                        PorsiMakan.Resolve();
                        break;

                    case 6:
                        Console.WriteLine("Game ");
                        Game.Resolve();
                        break;

                    case 7:
                        Console.WriteLine("Keranjang buah ");
                        KeranjangBuah.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("Bank ");
                        Bank.Resolve();
                        break;

                    case 9:
                        Console.WriteLine("Kartu GAG ");
                        Kartu.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("Penjumlahan prima dan fibonacci ");
                        PrimaFibonacci.Resolve();
                        break;

                    case 11:
                        Console.WriteLine("Number one ");
                        NumberOne.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan ");
                        break;
                }
            }
        }
    }
}
