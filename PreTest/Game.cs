﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PreTest
{
    class Game
    {
        public static void Resolve ()
        {
            Console.WriteLine("MENU");
            Console.WriteLine("1. Player main dahulu"+ "\t" + "2. Komputer main dahulu");

            Console.WriteLine("Masukan pilihanmu ");
            int pilihan = Convert.ToInt16(Console.ReadLine());

            Random nilai = new Random();
            int angkaKomputer = nilai.Next(0, 9);

            switch (pilihan)
            {
                case 1:
                    Console.WriteLine("Masukan angka ");
                    int angka = int.Parse(Console.ReadLine());

                    Console.WriteLine("Angka dari komputer " + angkaKomputer);
                    if (angka > angkaKomputer)
                    {
                        Console.WriteLine("You Win");
                    }
                    else if (angka < angkaKomputer)
                    {
                        Console.WriteLine("You Lose");
                    }
                    else if (angka == angkaKomputer)
                    {
                        Console.WriteLine("Draw");
                    }
                    break;

                case 2:
                    Console.WriteLine("Masukan angka ");
                    int angka2 = int.Parse(Console.ReadLine());

                    if (angka2 > angkaKomputer)
                    {
                        Console.WriteLine("You Win");
                    }
                    else if (angka2 < angkaKomputer)
                    {
                        Console.WriteLine("You Lose");
                    }
                    else if (angka2 == angkaKomputer)
                    {
                        Console.WriteLine("Draw");
                    }
                    Console.WriteLine("Angka dari komputer " + angkaKomputer);
                    break;
            }
        }
    }
}
