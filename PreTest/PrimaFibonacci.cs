﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PreTest
{
    class PrimaFibonacci
    {
        public static void Resolve ()
        {
            Console.WriteLine("Masukan panjang deret ");
            int panjang = int.Parse(Console.ReadLine());

            int[] primaArray = new int[panjang];

            int bilangan = 2;
            int index = 0;

            while (index < panjang)
            {
                int faktorBilanganPrima = 0;

                for (int i = 1; i <= bilangan; i++)
                {
                    if (bilangan % i == 0)
                    {
                        faktorBilanganPrima += 1;
                    }
                }

                if (faktorBilanganPrima == 2)
                {
                    Console.Write(bilangan + " ");
                    primaArray[index] = bilangan;
                    index++;
                }
                bilangan++;
            }
            Console.WriteLine();

            int[] fibonacciArray = new int[panjang];
            for (int i = 0; i < fibonacciArray.Length; i++)
            {
                if(i <= 1)
                {
                    fibonacciArray[i] = 1;
                }
                else
                {
                    fibonacciArray[i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
                }
                Console.Write(fibonacciArray[i] + " ");
            }
            Console.WriteLine();

            int[] resultArray = new int[panjang];

            for (int i = 0; i < resultArray.Length; i++)
            {
                resultArray[i] = primaArray[i] + fibonacciArray[i];
            }

            for (int i = 0; i < resultArray.Length; i++)
            {
                Console.Write(resultArray[i] + " ");
            }
            Console.WriteLine();
        }
    }
}
