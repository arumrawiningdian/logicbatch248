﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PreTest
{
    class KonversiVolume
    {
        public static void Resolve ()
        {
            Console.WriteLine("Pilihan konversi volume ");
            Console.WriteLine("1. Botol ");
            Console.WriteLine("2. Gelas ");
            Console.WriteLine("3. Teko ");
            Console.WriteLine("4. Cangkir ");

            Console.WriteLine("Pilih jenis konversi , misal botol ke cangkir (ketik : botol ke cangkir)");
            string pilihan = Console.ReadLine();

            double botol = 2;
            double teko = 25;
            double gelas = 2.5;
           
            switch (pilihan)
            {
                case "botol ke cangkir":
                    Console.WriteLine("Konversi botol ke cangkir ");
                    Console.WriteLine("Masukan jumlah botol ");
                    int inputBotol = int.Parse(Console.ReadLine());

                    double konversiBotol = (inputBotol * botol) * gelas ;
                    int konversiBotolKeCangkir = (int)konversiBotol;

                    Console.WriteLine("Jadi " + inputBotol + " botol : " + konversiBotolKeCangkir + " cangkir");
                    break;

                case "botol ke gelas":
                    Console.WriteLine("Konversi botol ke gelas ");
                    Console.WriteLine("Masukan jumlah botol ");
                    int inputBotol2 = int.Parse(Console.ReadLine());

                    double konversiBotol2 = (inputBotol2 * botol);
                    int konversiBotolKeGelas = (int)konversiBotol2;

                    Console.WriteLine("Jadi " + inputBotol2 + " botol : " + konversiBotolKeGelas + " gelas");
                    break;

                case "botol ke teko":
                    Console.WriteLine("Konversi botol ke teko ");
                    Console.WriteLine("Masukan jumlah teko ");
                    int inputBotol3 = int.Parse(Console.ReadLine());

                    double konversiBotol3 = ((inputBotol3 * botol) * gelas) / teko;

                    Console.WriteLine("Jadi " + inputBotol3 + " botol : " + konversiBotol3 + " teko");
                    break;

                case "gelas ke botol":
                    Console.WriteLine("Konversi gelas ke botol ");
                    Console.WriteLine("Masukan jumlah gelas ");
                    int inputGelas = int.Parse(Console.ReadLine());

                    double konversiGelas = inputGelas / botol;

                    Console.WriteLine("Jadi " + inputGelas + " gelas : " + konversiGelas + " botol");
                    break;

                case "gelas ke teko":
                    Console.WriteLine("Konversi gelas ke teko ");
                    Console.WriteLine("Masukan jumlah gelas ");
                    int inputGelas2 = int.Parse(Console.ReadLine());

                    double konversiGelas2 = (inputGelas2 * gelas) / teko;

                    Console.WriteLine("Jadi " + inputGelas2 + " gelas : " + konversiGelas2 + " teko");
                    break;

                case "gelas ke cangkir":
                    Console.WriteLine("Konversi gelas ke cangkir ");
                    Console.WriteLine("Masukan jumlah gelas ");
                    int inputGelas3 = int.Parse(Console.ReadLine());

                    double konversiGelas3 = inputGelas3 * gelas;

                    Console.WriteLine("Jadi " + inputGelas3 + " gelas : " + konversiGelas3 + " cangkir");
                    break;

                case "teko ke botol":
                    Console.WriteLine("Konversi teko ke botol ");
                    Console.WriteLine("Masukan jumlah teko ");
                    int inputTeko = int.Parse(Console.ReadLine());
                    
                    double konversiTeko = ((inputTeko * teko) / gelas) / botol;

                    Console.WriteLine("Jadi " + inputTeko + " teko : " + konversiTeko + " botol");
                    break;

                case "teko ke gelas":
                    Console.WriteLine("Konversi teko ke gelas ");
                    Console.WriteLine("Masukan jumlah teko ");
                    int inputTeko2 = int.Parse(Console.ReadLine());
;
                    double konversiTeko2 = (inputTeko2 * teko) / gelas;

                    Console.WriteLine("Jadi " + inputTeko2 + " teko : " + konversiTeko2 + " gelas");
                    break;

                case "teko ke cangkir":
                    Console.WriteLine("Konversi teko ke cangkir ");
                    Console.WriteLine("Masukan jumlah teko ");
                    int inputTeko3 = int.Parse(Console.ReadLine());
                    
                    double konversiTeko3 = inputTeko3 * teko;
                    int konversiTekoKeCangkir = (int)konversiTeko3;

                    Console.WriteLine("Jadi " + inputTeko3 + " teko : " + konversiTekoKeCangkir + " cangkir");
                    break;

                case "cangkir ke botol":
                    Console.WriteLine("Konversi cangkir ke botol ");
                    Console.WriteLine("Masukan jumlah cangkir ");
                    int inputCangkir = int.Parse(Console.ReadLine());
                    
                    double konversiCangkir = (inputCangkir / gelas) / botol;

                    Console.WriteLine("Jadi " + inputCangkir + " cangkir : " + konversiCangkir + " botol");
                    break;

                case "cangkir ke gelas":
                    Console.WriteLine("Konversi cangkir ke gelas ");
                    Console.WriteLine("Masukan jumlah cangkir ");
                    int inputCangkir2 = int.Parse(Console.ReadLine());

                    double konversiCangkir2 = inputCangkir2 / gelas;

                    Console.WriteLine("Jadi " + inputCangkir2 + " cangkir : " + konversiCangkir2 + " gelas");
                    break;

                case "cangkir ke teko":
                    Console.WriteLine("Konversi cangkir ke teko ");
                    Console.WriteLine("Masukan jumlah cangkir ");
                    int inputCangkir3 = int.Parse(Console.ReadLine());

                    double konversiCangkir3 = inputCangkir3 / teko;

                    Console.WriteLine("Jadi " + inputCangkir3 + " cangkir : " + konversiCangkir3 + " teko");
                    break;

                default:
                    Console.WriteLine("Inputan anda salah, masukan lagi ");
                    Console.ReadKey();

                    break;
            }
        }
    }
}
