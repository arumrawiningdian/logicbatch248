﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Day04
{
    class MiddleAsteriskTiga
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan kalimat ");
            string kalimat = Console.ReadLine();

            string[] kalimatArray = kalimat.Split(' ');

            for (int i = 0; i < kalimatArray.Length; i++)
            {
                if (kalimatArray[i].Length < 3)
                {
                    Console.Write(kalimatArray[i] + " ");
                }
                else
                {
                    Console.Write(kalimatArray[i].First() + "***" + kalimatArray[i].Last() + " ");
                }
            }
        }
    }
}