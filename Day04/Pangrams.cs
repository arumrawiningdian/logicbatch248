﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day04
{
    class Pangrams
    {
        public static void Resolve()
        {
            string kumpulanHuruf = "abcdefghijklmnopqrstuvwxyz";
            System.Console.WriteLine("Masukkan kalimat");
            string kalimatAwal = System.Console.ReadLine();

            //kalimatAwal = "The quick brown fox jumps over The lazy dogg";
            string kalimatLower = kalimatAwal.ToLower();

            int jumlah = 0;

            for (int i = 0; i < kumpulanHuruf.Length; i++)
            {
                for (int j = 0; j < kalimatLower.Length; j++)
                {
                    if (kumpulanHuruf[i] == kalimatLower[j])
                    {
                        jumlah++;
                        break;
                    }
                }
            }

            if (jumlah == 26)
            {
                System.Console.WriteLine("Pangrams ");
            }

            else
            {
                System.Console.WriteLine("Bukan Pangrams ");
            }
        }

    }
}
