﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day04
{
    class Utility
    {
        public static int[] ConvertStringToIntArray(string numbers)
        {
            string[] stringNumbersArray = numbers.Split(' ');
            int[] numbersArray = new int[stringNumbersArray.Length];

            //Convert to int
            for (int i = 0; i < numbersArray.Length; i++)
            {
                numbersArray[i] = int.Parse(stringNumbersArray[i]);
            }

            return numbersArray;
        }
    }
}
