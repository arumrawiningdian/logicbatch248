﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day04
{
    class CamelCase
    {
        public static void Resolve ()
        {
            Console.WriteLine("Masukan kalimat ");
            string kalimat = Console.ReadLine();

            int wordCount = 1;

            if (char.IsUpper(kalimat[0]))
            {
                Console.WriteLine("Bukan camelcase ");
            }
            else
            {
                for (int i = 0; i < kalimat.Length; i++)
                {
                    if (char.IsUpper(kalimat[i]))
                    {
                        wordCount++;
                    }
                }
                Console.WriteLine("Hasilnya " + wordCount);
            }
        }
    }
}
