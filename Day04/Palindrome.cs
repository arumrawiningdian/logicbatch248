﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Day04
{
    class Palindrome
    {
        public static void Resolve()
        {
            Console.WriteLine("Input Kalimat : ");
            string sentence = Console.ReadLine();

            char[] charArray = sentence.ToCharArray();
            Array.Reverse(charArray);

            string reverseSentence = new string(charArray);
            string conclusion = " ";

            Console.WriteLine("Kalimat sebelum dibalik : " + sentence);
            Console.WriteLine("Dibalik menjadi :" + reverseSentence);

            if (sentence == reverseSentence)
            {
                Console.WriteLine("Merupakan kalimat Palindrome.");
            }
            else
            {
                Console.WriteLine("Bukan merupakan kalimat Palindrome");
            }
        }
    }
}
